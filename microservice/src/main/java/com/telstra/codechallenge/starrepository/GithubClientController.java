package com.telstra.codechallenge.starrepository;

import com.telstra.codechallenge.exceptions.InvalidRequestException;
import com.telstra.codechallenge.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class GithubClientController {
    public static final int NO_OF_DAYS = 7;

    private GithubClientService githubClientService;

    public GithubClientController(GithubClientService githubClientService) {
        this.githubClientService = githubClientService;
    }

    @GetMapping("/get-hottest-git-repos/{noOfRepos}")
    public ResponseEntity<RepositoryResponse> getHottestRepos(@PathVariable int noOfRepos) {
        log.debug("for hottest repository input value of noOfRepos is " + noOfRepos);
        if (noOfRepos <= 0) {
            // TODO
            throw new InvalidRequestException("NO of Repos number should be greater then 0 !");
        }
        return ResponseEntity.ok(githubClientService.getHottestRepos(noOfRepos, NO_OF_DAYS));
    }

}

